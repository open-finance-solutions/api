FROM python:3.7

# We copy just the requirements.txt first to leverage Docker cache
COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT python manage.py serve --env staging
# ENTRYPOINT guvicorn main:app