from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Optional
from pydantic import BaseModel, EmailStr
from fastapi.params import Form
from pydantic.fields import Field
from pydantic.networks import HttpUrl


class OAuth2RequestForm:
    def __init__(
        self,
        grant_type: str = Form(None),
        username: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        password: Optional[str] = Form(None),
        # scope: str = Form(...),
        client_id: Optional[str] = Form(None),
        client_secret: Optional[str] = Form(None),
    ):
        self.grant_type = grant_type
        self.email = username if username else email
        self.password = password
        # self.scope = scope
        self.client_id = client_id
        self.client_secret = client_secret


class Token(BaseModel):
    access_token: str
    token_type: str
    expiration_date: datetime


class SignUpForm(BaseModel):
    email: EmailStr
    first_name: Optional[str]
    last_name: Optional[str]
    is_business: Optional[bool] = False
    password: str


class AvailableBanks(str, Enum):
    eurobank = "eurobank"
    alphabank = "alphabank"
    nbg = "nbg"
    piraeus = "piraeus"
    revolut = "revolut"


class ConsentCreate(BaseModel):
    bank_name: AvailableBanks
    callback_url: HttpUrl

    class Config:
        use_enum_values = True


class ConsentStatus(str, Enum):
    new = "new"
    account_connected = "account_connected"
    renewed = "renewed"


class Consent(BaseModel):
    id: str
    token: Optional[str]
    status: ConsentStatus = ConsentStatus.new
    bank_name: str
    connection_url: Optional[str]
    callback_url: str
    provider_id: str

    class Config:
        use_enum_values = True


class Account(BaseModel):
    id: str
    account_number: Optional[str]
    holder_name: str


class User(BaseModel):
    id: str
    email: EmailStr
    password: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    is_business: bool
    active: bool = True


class Transaction(BaseModel):
    provider_id: str
    amount: float
    description: str


class AfterbanksConsentCreateResponse(BaseModel):
    follow: Optional[str]
    consentId: Optional[str]
    message: Optional[str]


class AfterbanksHolder(BaseModel):
    role: str
    name: Optional[str]
    id: str


class AfterBanksTransaction(BaseModel):
    date: str
    date2: str
    amount: float
    balance: float
    description: str
    categoryId: int
    mcc: str
    creditorName: str
    debtorName: str
    transactionId: str


class AfterbanksAccount(BaseModel):
    product: str
    type: str
    description: str
    iban: Optional[str]
    pan: Optional[str]
    is_owner: int
    balance: float
    currency: str
    holders: List[AfterbanksHolder]
    # Available only when you query AB for transactions
    transactions: Optional[List[AfterBanksTransaction]]


class AfterbanksConsent(BaseModel):
    token: str
    consentId: str
    globalPosition: List[AfterbanksAccount]


class TaskStatus(str, Enum):
    new = "new"
    failed = "failed"
    completed = "completed"


class TransactionClassificationTaskCreate(BaseModel):
    settings: Optional[Dict[str, Any]]
    transactions: List[Transaction]


class TransactionClassificationTask(BaseModel):
    id: str
    settings: Optional[Dict[str, Any]]
    status: TaskStatus
    transactions: List[Transaction]
