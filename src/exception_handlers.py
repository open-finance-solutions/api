import traceback
from fastapi.encoders import jsonable_encoder
from fastapi.exception_handlers import request_validation_exception_handler, http_exception_handler
from fastapi.exceptions import HTTPException, RequestValidationError
from starlette.requests import Request
from starlette.responses import JSONResponse

from src.logger import log_exception


async def ofs_validation_exception_handler(request: Request, exc: RequestValidationError):
    log_exception(exc)
    return await request_validation_exception_handler(request, exc)


async def ofs_http_exception_handler(request: Request, exc: HTTPException):
    log_exception(exc)
    return await http_exception_handler(request, exc)


async def ofs_generic_exception_handler(request: Request, exc: Exception):
    log_exception(exc)
    return JSONResponse(
        status_code=500, content=jsonable_encoder({"detail": traceback.format_exc()})
    )
