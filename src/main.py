import logging
from fastapi import FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from neomodel import db
from starlette.requests import Request
from starlette.responses import JSONResponse
from src.api_v1 import endpoints
from src.exception_handlers import (
    ofs_generic_exception_handler,
    ofs_http_exception_handler,
    ofs_validation_exception_handler,
)
from src.settings import get_settings
from fastapi.openapi.utils import get_openapi
import json
from src.api_v1.docs import description, tags_metadata
import time
from src.logger import get_loki_logger


settings = get_settings()

app = FastAPI(title=settings.APP_NAME, redoc_url="/")


@app.middleware("http")
async def send_endpoint_analytics(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    logger = get_loki_logger()
    flow_id = (
        request.scope["endpoint"].flow_id
        if hasattr(request.scope["endpoint"], "flow_id")
        else str(request.url)
    )
    logger.info(
        round(process_time * 1000, 2),
        extra={
            "tags": {"level": logging.getLevelName(logging.INFO), "flow": flow_id},
        },
    )
    return response


app.add_exception_handler(RequestValidationError, ofs_validation_exception_handler)
app.add_exception_handler(HTTPException, ofs_http_exception_handler)
app.add_exception_handler(Exception, ofs_generic_exception_handler)


@app.on_event("startup")
def startup():
    db.set_connection(settings.DB_CONNECTION_STRING)


# TODO: disconnect from db

app.include_router(endpoints.v1)


def app_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Open Finance Solutions",
        version="",
        routes=app.routes,
        description=description,
        tags=tags_metadata,
    )
    with open("openapi.json", "r") as openapi:
        openapi = json.load(openapi)
        logo = openapi["info"]["x-logo"]
    openapi_schema["info"]["x-logo"] = logo

    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = app_openapi
