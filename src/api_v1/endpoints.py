from logging import Logger
import logging
from time import process_time, time
from src.logger import get_loki_logger, log_afterbanks_action
from src.settings import get_settings
from typing import List
from src.actions.get_transactions import GetTransactions
from src.actions.get_accounts import GetAccounts
from src.actions.get_consents import GetConsents
from src.actions.create_consent_afterbanks import CreateConsentAfterbanks
from src.actions.create_consent import CreateConsent
from src.actions.authenticate import Authenticate
from src.actions.sign_up import SignUp
from src.schemas import (
    Account,
    AfterbanksConsent,
    Consent,
    ConsentCreate,
    OAuth2RequestForm,
    SignUpForm,
    TaskStatus,
    Token,
    Transaction,
    TransactionClassificationTask,
    TransactionClassificationTaskCreate,
    User,
)
from fastapi import APIRouter, Depends
from starlette import status
from src.middleware.get_active_user import get_active_user
from src.utils import flow

settings = get_settings()
router = APIRouter()


@flow(id="health")
@router.get(
    "/health",
    tags=["Health"],
    summary="Health check",
    response_description="Status of the service",
)
def health_check():
    """Returns the health status of the service"""
    return dict(status="OK")


@flow(id="Sign up")
@router.post(
    "/auth/sign-up",
    tags=["Authentication"],
    response_model=User,
    summary="Sign up",
)
async def sign_up(sign_up_form: SignUpForm):
    """Sign up / create a new user"""
    return await SignUp().execute(sign_up_form)


@flow(id="Get access token")
@router.post(
    "/auth/token",
    tags=["Authentication"],
    summary="Get access token",
    response_model=Token,
)
async def get_access_token(form_data: OAuth2RequestForm = Depends()):
    """
    Exchange credentials for an access token.
    Access tokens are used to access API resources.
    Note that the data is sent as **application/x-www-form-urlencoded**.
    """
    return await Authenticate().execute(form_data)


@flow(id="Get current user")
@router.post(
    "/auth/me",
    tags=["Authentication"],
    summary="Get current user",
    response_model=User,
)
async def get_current_user(active_user: User = Depends(get_active_user)):
    """Get current authenticated user's data."""
    return active_user


@flow(id="Get consents")
@router.get(
    "/consents",
    tags=["Account Information"],
    summary="Get consents",
    response_model=List[Consent],
)
async def get_consents(active_user: User = Depends(get_active_user)):
    """Get consents of the user."""
    return await GetConsents().execute(active_user)


@flow(id="Create consent / Connect bank account")
@router.post(
    "/consents",
    tags=["Account Information"],
    summary="Create consent / Connect bank account",
    response_model=Consent,
)
async def create_consent(
    consent_create: ConsentCreate,
    active_user: User = Depends(get_active_user),
):

    start = time()

    def on_afterbanks_response(timestamp):
        process_time = start - timestamp
        log_afterbanks_action(process_time, "CreateConsent")

    """
    Create new consent. Use this end to connect your bank account.
    After a consent has been created, use the `connection_url` to redirect to the bank where you can connect you bank.
    After your bank has been connected successfully you will be able to retreive your accounts and transactions through the respective endpoints.
    """
    return await CreateConsent(on_afterbanks_response=on_afterbanks_response).execute(
        active_user, consent_create
    )


@flow(id="Renew consent")
@router.put("/consents/{consent_id}/renew-consent", tags=["Account Information"])
async def WIP_renew_consent(active_user: User = Depends(get_active_user)):
    pass


@flow(id="Delete consent")
@router.delete("/consents/{consent_id}", tags=["Account Information"])
async def WIP_delete_consent(active_user: User = Depends(get_active_user)):
    pass


@flow(id="Get accounts")
@router.get(
    "/accounts",
    tags=["Account Information"],
    summary="Get accounts",
    response_model=List[Account],
)
async def get_accounts(active_user: User = Depends(get_active_user)):
    """
    Get user's bank accounts. The Account object represents a bank account. Features of the object may vary between different account types.
    """
    return await GetAccounts().execute(active_user)


@flow(id="Get transactions")
@router.get(
    "/accounts/{account_id}/transactions",
    tags=["Account Information"],
    summary="Get transactions",
    response_model=List[Transaction],
    operation_id="test",
)
async def get_transactions(account_id: str, active_user: User = Depends(get_active_user)):
    """
    Get account transactions. The Transaction object represents a bank transaction.
    """
    return await GetTransactions().execute(active_user, account_id)


@flow(id="Create transaction classification")
@router.post(
    "/tasks/transaction-classification/",
    tags=["Bring Your Own Data"],
    summary="Create transaction classification task",
    response_model=TransactionClassificationTask,
    status_code=status.HTTP_202_ACCEPTED,
)
async def create_transaction_classification_task(
    task: TransactionClassificationTaskCreate,
    active_user: User = Depends(get_active_user),
):
    """
    Create a transaction classification task. This new task will run asynchronously and when it is completed
    the results will become available from the `tasks/transaction-classification/{task_id}` endpoint.
    """
    return TransactionClassificationTask(id="fake-id", status=TaskStatus.new, transactions=[])


@flow(id="Get transaction classification")
@router.get(
    "/tasks/transaction-classification/{task_id}",
    tags=["Bring Your Own Data"],
    summary="Get transaction classification task",
    response_model=TransactionClassificationTask,
)
async def get_transaction_classification_task(
    task_id: str, active_user: User = Depends(get_active_user)
):
    """
    Get the results of a transaction classification task. You may poll this endpoint until the task
    is completed. When the task is completed the status property will have the value: `completed`.
    """
    return TransactionClassificationTask(id="fake-id", status=TaskStatus.new, transactions=[])


@flow(id="Create afterbanks consent")
@router.post(
    "/users/{user_id}/consent/afterbanks",
    tags=["Misc"],
    include_in_schema=settings.ENV == "dev",
)
async def afterbanks_create_consent(
    user_id: str,
    consent_afterbanks: AfterbanksConsent,
):

    start = time()

    def on_fetch_accounts_with_transactions(timestamp):
        process_time = start - timestamp
        log_afterbanks_action(process_time, "CreateConsentAfterbanks")

    """
    This route is used by Afterbanks to give us the consent after the user has been connected to a bank.
    After this we get all the users financial info.
    """
    return await CreateConsentAfterbanks(
        on_fetch_accounts_with_transactions=on_fetch_accounts_with_transactions
    ).execute(user_id, consent_afterbanks)


v1 = APIRouter()
v1.include_router(router, prefix="/v1")
