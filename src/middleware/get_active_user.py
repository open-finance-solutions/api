from src.schemas import User
from fastapi.security.oauth2 import OAuth2PasswordBearer
from fastapi import Depends, status, HTTPException
from src.settings import get_settings
from jose import JWTError, jwt

# scopes = dict(api="Access public api", dashboard="Access apps")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="v1/auth/token")
settings = get_settings()


def get_active_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
    )

    try:
        payload = _decode_jwt(token)
        if payload is None:
            raise credentials_exception
        return User(**payload)

    except JWTError as e:
        print(e)
        raise credentials_exception


def _decode_jwt(token: str):
    return jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
