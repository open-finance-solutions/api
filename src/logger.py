import logging
from typing import Union
from fastapi.exceptions import HTTPException, RequestValidationError
import logging_loki
from multiprocessing import Queue
from src.settings import get_settings

settings = get_settings()

handler = logging_loki.LokiQueueHandler(
    Queue(-1),
    url=settings.LOKI,
    # TODO: Create versioning for the logger. We need different logs
    # for v1 and v2
    tags={"application": "OFS", "version": "v1"},
    # Version is used internally by the client. It is irrelevant
    # to the api version
    version="1",
)


logger = logging.getLogger("loki")
logger.setLevel(level=logging.INFO)
logger.addHandler(handler)


def get_loki_logger():
    return logging.getLogger("loki")


def log_afterbanks_action(time: float, action: str):
    logger.info(
        time,
        extra={
            "tags": {
                "level": logging.getLevelName(logging.INFO),
                "action": action,
            },
        },
    )


def log_exception(exc: Union[RequestValidationError, HTTPException, Exception]):
    logger = get_loki_logger()
    logger.exception(
        exc,
        extra={
            "tags": {"level": logging.getLevelName(logging.ERROR)},
        },
    )
