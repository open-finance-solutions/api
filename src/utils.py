def flow(id: str):
    def decorator(fn):
        fn.flow_id = id
        return fn

    return decorator
