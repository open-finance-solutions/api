from src.settings import get_settings
from neomodel.properties import BooleanProperty, FloatProperty
from neomodel import (
    StructuredNode,
    StringProperty,
    UniqueIdProperty,
    RelationshipTo,
    config,
)
import secrets


def create_id():
    return secrets.token_hex(24)


settings = get_settings()
config.DATABASE_URL = settings.DB_CONNECTION_STRING


class Transaction(StructuredNode):
    uid = StringProperty(default=create_id)
    provider_id = StringProperty(unique_index=True)
    amount = FloatProperty()
    description = StringProperty()


class Account(StructuredNode):
    uid = StringProperty(default=create_id)
    account_number = StringProperty(unique_index=True)
    holder_name = StringProperty()

    transactions = RelationshipTo(Transaction, "HAS_TRANSACTION")


class Consent(StructuredNode):
    uid = StringProperty(default=create_id)
    token = StringProperty()
    status = StringProperty()
    bank_name = StringProperty()
    connection_url = StringProperty()
    callback_url = StringProperty()
    provider_id = StringProperty(unique_index=True)

    accounts = RelationshipTo(Account, "IS_GIVEN_FOR_ACCOUNT")


class User(StructuredNode):
    uid = StringProperty(default=create_id)
    email = StringProperty(unique_index=True)
    password = StringProperty()
    first_name = StringProperty()
    last_name = StringProperty()
    is_business = BooleanProperty(default=False)
    active = BooleanProperty(default=True)

    consents = RelationshipTo(Consent, "HAS_GIVEN_CONSENT")
    accounts = RelationshipTo(Account, "HAS_ACCOUNT")
    transactions = RelationshipTo(Transaction, "HAS_TRANSACTION")
