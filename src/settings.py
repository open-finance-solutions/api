from typing import Optional
from pydantic import BaseSettings
import os
from functools import lru_cache


class Settings(BaseSettings):
    APP_NAME: str
    ENV: str

    DB_CONNECTION_STRING: str

    SECRET_KEY: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int
    ALGORITHM: str

    AFTERBANKS_SERVICE_KEY: str
    AFTERBANKS_BASE_URL: str
    AFTERBANKS_CONSENT_CALLBACK: str

    LOKI: str

    SUPPORTED_BANKS = dict(GR=["alphabank", "eurobank", "nbg", "piraeus", "revolut"])

    class Config:
        env_file = f".env.{os.getenv('ENV')}" or ".env.dev"


@lru_cache()
def get_settings():
    return Settings()
