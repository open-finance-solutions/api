from typing import List
from .action import ActionBase
from src.schemas import Account, User


class GetAccounts(ActionBase):
    async def execute(self, current_user: User) -> List[Account]:
        user_in_db = self.entities.User.nodes.get_or_none(uid=current_user.id)
        accounts_in_db = user_in_db.accounts.all()
        return [
            Account(id=a.uid, account_number=a.account_number, holder_name=a.holder_name)
            for a in accounts_in_db
        ]
