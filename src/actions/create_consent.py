from time import time
from typing import Callable
from src.schemas import AfterbanksConsentCreateResponse, Consent, ConsentCreate, ConsentStatus, User
from datetime import datetime
from dateutil.relativedelta import relativedelta
from .action import ActionBase
from fastapi import HTTPException, status


class ConsentPreDB(ConsentCreate):
    status = ConsentStatus.new.value
    provider_id: str
    connection_url: str


class CreateConsent(ActionBase):
    afterbanks_response_cb: Callable

    def __init__(self, on_afterbanks_response: Callable):
        self.afterbanks_response_cb = on_afterbanks_response
        super().__init__()

    async def execute(self, current_user: User, consent_create: ConsentCreate) -> Consent:
        # TODO: Check if user exists
        consent_ab = await self.create_afterbanks_consent(current_user, consent_create)
        consent_pre_db = ConsentPreDB(
            **consent_create.dict(),
            provider_id=consent_ab.consentId,
            connection_url=consent_ab.follow,
        )
        consent_in_db = await self.save_consent(current_user, consent_pre_db)
        return Consent(**consent_in_db.__dict__)

    async def save_consent(self, current_user: User, consent_pre_db: ConsentPreDB):
        user_in_db = self.entities.User.nodes.get_or_none(uid=current_user.id)
        new_consent = self.entities.Consent(**consent_pre_db.dict())
        new_consent.save()
        user_in_db.consents.connect(new_consent)
        return new_consent

    async def create_afterbanks_consent(
        self, current_user: User, consent_create: ConsentCreate
    ) -> AfterbanksConsentCreateResponse:
        # Afterbanks calls this consent. We use this as a bank redirect url.
        url = f"{self.settings.AFTERBANKS_BASE_URL}/consent/get/"
        callback = self.settings.AFTERBANKS_CONSENT_CALLBACK
        # Webhook is used on dev env (because AB cant call your localhost) but on live envs the user id is needed.
        current_env_callback = (
            callback
            if self.settings.ENV == "dev"
            else callback.replace("{user_id}", current_user.id)
        )
        data = dict(
            servicekey=self.settings.AFTERBANKS_SERVICE_KEY,
            service=consent_create.bank_name,
            grantType="read",
            yourConsentCallback=current_env_callback,
            urlRedirect=consent_create.callback_url,
            validUntil=(datetime.now() + relativedelta(days=90)).strftime("%d-%m-%Y"),
        )

        r = await self.http.post(url, data=data)

        if self.afterbanks_response_cb:
            timestamp = time()
            self.afterbanks_response_cb(timestamp)

        consent_ab = AfterbanksConsentCreateResponse(**r.json())

        # TODO: Better Error handling. dont send the AB message
        if consent_ab.message:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=consent_ab.message)

        return consent_ab
