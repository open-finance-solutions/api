from typing import List

from fastapi.exceptions import HTTPException
from starlette import status
from .action import ActionBase
from src.schemas import User, Transaction


class GetTransactions(ActionBase):
    async def execute(self, current_user: User, account_id: str) -> List[Transaction]:
        account_in_db = self.entities.Account.nodes.get_or_none(uid=account_id)
        if not account_in_db:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Account not found")
        transactions_in_db = account_in_db.transactions.all()
        return [Transaction(**a.__dict__) for a in transactions_in_db]
