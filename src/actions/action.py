from passlib.context import CryptContext
from src.settings import Settings, get_settings
from src.adapters import database_client, http_client


class Entities:
    User = database_client.User
    Consent = database_client.Consent
    Account = database_client.Account
    Transaction = database_client.Transaction


class ActionBase:
    def __init__(self):
        self.settings = get_settings()
        self.http = http_client.client
        self.entities = Entities()
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
