from .action import ActionBase
from src.schemas import SignUpForm, User


class SignUp(ActionBase):
    async def execute(self, user_create: SignUpForm) -> User:
        normalized_user = self.normalize(user_create)
        new_user = self.save_user(normalized_user)
        return new_user

    def normalize(self, user_create: SignUpForm) -> SignUpForm:
        user_create.email = user_create.email.lower()
        user_create.password = self.hash_pass(user_create.password)
        return user_create

    def hash_pass(self, password: str) -> str:
        return self.pwd_context.hash(password)

    def check_if_email_in_use(self):
        # TODO:
        pass

    def save_user(self, user_create: SignUpForm) -> User:
        new_user = self.entities.User(**user_create.dict()).save()
        return User(id=new_user.uid, **user_create.dict())
