from time import time
from typing import Any, Callable, List
from fastapi.exceptions import HTTPException
from datetime import datetime
from dateutil.relativedelta import relativedelta
from .action import ActionBase
from src.schemas import (
    AfterBanksTransaction,
    AfterbanksAccount,
    AfterbanksConsent,
    ConsentStatus,
)


class CreateConsentAfterbanks(ActionBase):
    accounts_with_transactions_fetched: Callable

    def __init__(self, on_fetch_accounts_with_transactions):
        self.accounts_with_transactions_fetched = on_fetch_accounts_with_transactions
        super().__init__()

    async def execute(self, user_id: str, consent_afterbanks: AfterbanksConsent):
        user_in_db = self.get_user(user_id)

        # TODO: Check if this needs to happen last
        consent_in_db = self.update_consent(consent_afterbanks)

        accounts_with_transactions = await self.get_accounts_with_transactions(
            consent_in_db, consent_afterbanks
        )

        self.save_accounts_and_transactions(user_in_db, consent_in_db, accounts_with_transactions)

        return dict(result="OK")

    def update_consent(self, consent_afterbanks: AfterbanksConsent):
        consent = self.entities.Consent.nodes.get_or_none(provider_id=consent_afterbanks.consentId)
        # Use the enum value because this is not a pydantic base model
        consent.status = ConsentStatus.account_connected.value
        consent.token = consent_afterbanks.token
        consent.save()
        return consent

    def transform_afterbanks_accounts(self, afterbanks_accounts: List[AfterbanksAccount]):
        accounts = [
            dict(
                account_number=a.iban or a.pan,
                holder_name=a.holders[0].name if len(a.holders) else None,
            )
            for a in afterbanks_accounts
        ]
        return accounts

    def transform_afterbanks_transactions(
        self, afterbanks_transactions: List[AfterBanksTransaction]
    ):
        transactions = [
            dict(
                provider_id=t.transactionId,
                amount=t.amount,
                description=t.description,
            )
            for t in afterbanks_transactions
        ]
        return transactions

    def get_user(self, user_id):
        user = self.entities.User.nodes.get_or_none(uid=user_id)
        if user is None:
            raise HTTPException(status_code=400, detail="No user found with this id")
        return user

    async def get_accounts_with_transactions(self, consent_in_db, ab_consent: AfterbanksConsent):
        # TODO: Maybe we need datetime.utcnow() ?
        start_date = (datetime.now() - relativedelta(years=1)).strftime("%d-%m-%Y")
        url = f"{self.settings.AFTERBANKS_BASE_URL}/transactions/"
        # Either use account_number or PAN
        account_ids = [account.product or "" for account in ab_consent.globalPosition]
        accounts = []
        for index, id in enumerate(account_ids):
            data = dict(
                servicekey=self.settings.AFTERBANKS_SERVICE_KEY,
                token=consent_in_db.token,
                products=id,
                startDate=start_date,
            )
            r = await self.http.post(url, data=data)

            if self.accounts_with_transactions_fetched:
                timestamp = time()
                self.accounts_with_transactions_fetched(timestamp)

            accounts.append(r.json()[index])
        return [AfterbanksAccount(**account) for account in accounts]

    def save_accounts_and_transactions(
        self, user_in_db, consent_in_db, accounts_with_transactions: List[AfterbanksAccount]
    ):
        for a in accounts_with_transactions:
            accounts = self.transform_afterbanks_accounts([a])
            transactions = self.transform_afterbanks_transactions(a.transactions or [])
            accounts_in_db = self.entities.Account.create_or_update(
                *accounts, relationship=user_in_db.accounts
            )
            transactions_in_db = self.entities.Transaction.create_or_update(
                *transactions, relationship=user_in_db.transactions
            )
            consent_in_db.accounts.connect(accounts_in_db[0])
            for t_in_db in transactions_in_db:
                accounts_in_db[0].transactions.connect(t_in_db)
