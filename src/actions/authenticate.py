from .action import ActionBase
from src.schemas import OAuth2RequestForm, Token, User
from passlib.context import CryptContext
from fastapi import HTTPException, status
from datetime import datetime, timedelta
from jose import jwt


class Authenticate(ActionBase):
    async def execute(self, form_data: OAuth2RequestForm) -> Token:
        user_in_db = self.get_user_by_email(form_data)
        self.validate(form_data, user_in_db)

        minutes = self.settings.ACCESS_TOKEN_EXPIRE_MINUTES
        expiration = datetime.utcnow() + timedelta(minutes=minutes)
        access_token = self.create_access_token(user_in_db)

        return Token(access_token=access_token, token_type="Bearer", expiration_date=expiration)

    def validate(self, form_data: OAuth2RequestForm, user: User):
        status_code = status.HTTP_401_UNAUTHORIZED
        if not user:
            raise HTTPException(status_code=status_code, detail="No user found with this email")

        if not user.active:
            raise HTTPException(status_code=status_code, detail="User is inactive")

        if not self.validate_password(form_data.password, user.password):
            raise HTTPException(status_code=status_code, detail="Wrong password")

    def validate_scopes(self):
        # TODO
        pass

    def validate_password(self, plain_password, hashed_password):
        return self.pwd_context.verify(plain_password, hashed_password)

    def get_user_by_email(self, form_data: OAuth2RequestForm) -> User:
        user = self.entities.User.nodes.get_or_none(email=form_data.email)
        return user

    def create_access_token(self, user_in_db) -> str:
        to_encode = dict(
            id=user_in_db.uid,
            email=user_in_db.email,
            first_name=user_in_db.first_name,
            last_name=user_in_db.last_name,
            is_business=user_in_db.is_business,
        )
        encoded_jwt = jwt.encode(
            to_encode,
            self.settings.SECRET_KEY,
            algorithm=self.settings.ALGORITHM,
        )
        return encoded_jwt
