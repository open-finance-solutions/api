from typing import List
from .action import ActionBase
from src.schemas import Consent, User


class GetConsents(ActionBase):
    async def execute(self, current_user: User) -> List[Consent]:
        user_in_db = self.entities.User.nodes.get_or_none(uid=current_user.id)
        consents_in_db = user_in_db.consents.all()
        return [Consent(**c.__dict__) for c in consents_in_db]
