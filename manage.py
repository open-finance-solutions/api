#! ./venv/bin/python

import os
import typer
from typing import Optional
from enum import Enum

app = typer.Typer()


class Environment(str, Enum):
    dev = "dev"
    test = "test"
    staging = "staging"
    production = "production"


@app.command()
def serve(env: Optional[Environment] = Environment.dev, host: str = "0.0.0.0"):
    typer.echo(f"\nRunning API | Environment: {env} 🚀 \n")
    os.system(f"ENV='{env}' uvicorn src.main:app --reload --host {host}")


@app.command()
def apply_constraints(env: Optional[Environment] = Environment.dev):
    typer.echo(f"\nApplying constraints and indexes | Environment: {env} 🚀 \n")
    # TODO: Load env settings
    os.system(f"neomodel_remove_labels --db bolt://neo4j:ofsofs@localhost:7687")
    os.system(
        f"ENV='{env}' neomodel_install_labels src.adapters.database_client --db bolt://neo4j:ofsofs@localhost:7687"
    )


if __name__ == "__main__":
    app()
